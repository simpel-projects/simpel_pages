# Generated by Django 3.2.13 on 2022-04-14 17:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('simpel_pages', '0008_auto_20220414_1649'),
    ]

    operations = [
        migrations.CreateModel(
            name='HTMLBlock',
            fields=[
                ('block_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='simpel_pages.block')),
                ('content', models.TextField(verbose_name='content')),
            ],
            options={
                'verbose_name': 'HTML Block',
                'verbose_name_plural': 'HTML Blocks',
            },
            bases=('simpel_pages.block',),
        ),
    ]
