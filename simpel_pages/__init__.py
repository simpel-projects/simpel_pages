from simpel_pages.version import get_version

VERSION = (0, 1, 10, "final", 0)

__version__ = get_version(VERSION)
