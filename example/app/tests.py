from django.test import TestCase

from example.app.models import SamplePage


class TestSample(TestCase):
    def test_calc(self):
        self.assertEqual(1 + 1, 2)

    def test_create(self):
        p = SamplePage(title="some how")
        p.save()
