from simpel_pages.models import Page
from simpel_pages.routable import RoutablePageMixin, route


class Blog(RoutablePageMixin, Page):
    def get_context_data(self, request, **kwargs):
        context = super().get_context_data(request, **kwargs)
        context["pages"] = self.childs.all().instance_of(Post)
        return context

    @route(r"^tag/(?P<tag>[-\w]+)/$")
    def tag(self, request, tag):
        queryset = Post.objects.filter(parent=self, tags__slug=tag)
        return self.render(
            request,
            template="blog_category.html",
            context_overrides={
                "pages": queryset.all(),
            },
        )

    @route(r"^category/(?P<category>[-\w]+)/$", name="blog_category")
    def category(self, request, category):
        queryset = Post.objects.filter(parent=self, category__slug=category)
        return self.render(
            request,
            template="blog_category.html",
            context_overrides={
                "pages": queryset.all(),
            },
        )


class Post(Page):
    pass
