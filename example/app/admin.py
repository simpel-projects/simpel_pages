from django.contrib import admin

from simpel_pages.admin import PageAdmin

from .models import Blog, Post

admin.site.register(Blog, PageAdmin)


@admin.register(Post)
class PostAdmin(PageAdmin):
    page_fields = PageAdmin.page_fields
    fieldsets = (
        (None, {"fields": page_fields}),
        PageAdmin.seo_settings,
    )
