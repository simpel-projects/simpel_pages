# Simpel Pages

Simple Django Content Management System, an open-source alternative to Django CMS, Wagtail etc.

<p align="center">
  <a href="https://heroku.com/deploy?template=https://gitlab.com/simpel-projects/simpel_pages/tree/master" alt="Deploy to Heroku">
     <img width="150" alt="Deploy" src="https://www.herokucdn.com/deploy/button.svg"/>
  </a>
</p>
